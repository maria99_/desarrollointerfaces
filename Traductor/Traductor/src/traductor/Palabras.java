package traductor;

public class Palabras {

	private String[] espanol;
	private String[] ingles;
	private String[] frances;
	
	public Palabras() {
		espanol = new String []{"Hola", "Adios", "Pan", "Luna", "Taza", "Rosa", "Plumas", "Jarron", "Limpio", "Malo", "Cine", "Supermercado"};
		ingles = new String []{"Hello", "Bye", "Bread", "Moon", "Cup", "Pink", "Feathers", "Vase", "Clean", "Bad", "Cinema", "Supermarket"};
		frances = new String []{"Hola", "Adios", "Pain", "Lune","Coupe", "Rose", "Plumes", "Vase", "Propre", "Mauvais gar�on", "Film", "Supermarch�"};	
	}

	public String[] getEspanol() {
		return espanol;
	}

	public String[] getFrances() {
		return frances;
	}

	public String[] getIngles() {
		return ingles;
	}
	

}
