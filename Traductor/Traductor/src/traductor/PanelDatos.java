package traductor;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class PanelDatos extends JPanel implements ActionListener{

	private GridBagLayout layout;
	private GridBagConstraints config;
	
	private Paneles panel1;
	private Paneles panel2;
	private Paneles panel3;
	
	private JTextArea cuadro1;
	private JTextArea cuadro2;
	
	private JLabel centrado;
	
	private JButton boton;
	
	private JRadioButton idioma1;
	private JRadioButton idioma2;
	private JRadioButton idioma3;
	private JRadioButton idioma4;
	private JRadioButton idioma5;
	private JRadioButton idioma6;
	
	private Palabras palabra;
	

	public PanelDatos() {

		palabra = new Palabras();
		
		centrado = new JLabel("Traduccion:");
		layout = new GridBagLayout();
		config = new GridBagConstraints();
		this.setLayout(layout);

		agregarPaneles();
		
		agregarTexto();
		agregarBoton();

		crearRadioButton();
		juntarRadioButton();
		agregarRadioButton();

	}


	private void agregarBoton() {
		boton = new JButton("Traduccion");
		boton.addActionListener((ActionListener) this);
		
		config.gridx = 1;
		config.gridy = 4;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.fill = GridBagConstraints.HORIZONTAL;
		config.weightx = 0.0;
		config.weighty = 0.0;
		config.insets = new Insets(5, 5, 5, 5);
		panel1.add(boton, config);
	}

	

	private void agregarTexto() {
		cuadro1 = new JTextArea();
		cuadro2 = new JTextArea();
		
		config.gridx = 0;
		config.gridy = 0;
		config.gridwidth = 3;
		config.gridheight = 1;
		config.fill = GridBagConstraints.BOTH;
		config.insets = new Insets(0, 0, 0, 0);
		config.weightx = 1.0;
		config.weighty = 1.0;
		panel1.add(cuadro1, config);

		config.gridx = 0;
		config.gridy = 1;
		config.gridwidth = 3;
		config.gridheight = 1;
		config.insets = new Insets(0, 0, 0, 0);
		panel1.add(centrado, config);

		config.gridx = 0;
		config.gridy = 2;
		config.gridwidth = 3;
		config.gridheight = 1;
		config.insets = new Insets(0, 0, 0, 0);
		panel1.add(cuadro2, config);

	}

	

	private void agregarRadioButton() {
		config.gridx = 0;
		config.gridy = 0;
		config.gridwidth = 3;
		config.gridheight = 1;
		config.fill = GridBagConstraints.BOTH;
		config.insets = new Insets(5, 5, 5, 5);
		panel2.add(idioma1, config);

		config.gridx = 0;
		config.gridy = 1;
		panel2.add(idioma2, config);

		config.gridx = 0;
		config.gridy = 2;
		panel2.add(idioma3, config);

		config.gridx = 0;
		config.gridy = 0;
		panel3.add(idioma4, config);

		config.gridx = 0;
		config.gridy = 1;
		panel3.add(idioma5, config);

		config.gridx = 0;
		config.gridy = 2;
		panel3.add(idioma6, config);

	}

	private void juntarRadioButton() {

		ButtonGroup idiomaOriginal = new ButtonGroup();
		idiomaOriginal.add(idioma1);
		idiomaOriginal.add(idioma2);
		idiomaOriginal.add(idioma3);

		ButtonGroup idiomaDestino = new ButtonGroup();

		idiomaDestino.add(idioma4);
		idiomaDestino.add(idioma5);
		idiomaDestino.add(idioma6);

	}

	private void crearRadioButton() {
		idioma1 = new JRadioButton("Frances");
		idioma2 = new JRadioButton("Espa�ol");
		idioma3 = new JRadioButton("Ingles");
		idioma4 = new JRadioButton("Frances");
		idioma5 = new JRadioButton("Espa�ol");
		idioma6 = new JRadioButton("Ingles");

		idioma1.addActionListener((ActionListener) this);
		idioma2.addActionListener((ActionListener) this);
		idioma3.addActionListener((ActionListener) this);
		idioma4.addActionListener((ActionListener) this);
		idioma5.addActionListener((ActionListener) this);
		idioma6.addActionListener((ActionListener) this);

	}

	private void agregarPaneles() {
		panel1 = new Paneles(Color.RED, "Traduccion");
		panel2 = new Paneles(Color.GREEN, "Idioma Origen");
		panel3 = new Paneles(Color.BLACK, "Idioma Destino");
		
		GridBagConstraints config = new GridBagConstraints();
		
		config.gridx = 0;
		config.gridy = 0;
		config.gridwidth = 1;
		config.gridheight = 2;
		config.fill = GridBagConstraints.BOTH;
		config.insets = new Insets(5, 5, 5, 5);
		this.add(panel1, config);

		config.gridx = 1;
		config.gridy = 0;
		config.gridwidth = 1;
		config.gridheight = 1;
		this.add(panel2, config);

		config.gridx = 1;
		config.gridy = 1;
		config.gridwidth = 1;
		config.gridheight = 1;
		this.add(panel3, config);

	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == boton) {
			if (idioma1.isSelected()) {
				escribirPalabra(buscarPalabra(palabra.getEspanol(), cuadro1.getText()));
			}
			if (idioma2.isSelected()) {
				escribirPalabra(buscarPalabra(palabra.getIngles(), cuadro1.getText()));
			}
			if (idioma3.isSelected()) {
				escribirPalabra(buscarPalabra(palabra.getFrances(), cuadro1.getText()));
			}
		}
	}

	private void escribirPalabra(int posicion) {
		if (posicion != -1) {
			if (idioma4.isSelected()) {
				cuadro2.setText(palabra.getEspanol()[posicion]);
			}
			if (idioma5.isSelected()) {
				cuadro2.setText(palabra.getIngles()[posicion]);
			}
			if (idioma6.isSelected()) {
				cuadro2.setText(palabra.getFrances()[posicion]);
			}
		} else {
			cuadro2.setText("La palabra no se encuentra en el diccionario");
		}

	}

	private int buscarPalabra(String[] in, String string) {
		int posicion = -1;
		for (int i = 0; i < in.length - 1; i++) {
			if (in[i].equalsIgnoreCase(string.trim())) {
				posicion = i;
				return posicion;
			}
		}
		return posicion;
	}	
}
