package traductor;

import java.awt.Color;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class Paneles extends JPanel{
	
	public Paneles(Color color, String string) {
		this.setLayout(new GridBagLayout());
		this.setBorder(new LineBorder(color, 2));
		this.setBorder(new TitledBorder(new LineBorder(Color.BLACK, 1), string));
	}
	

}
