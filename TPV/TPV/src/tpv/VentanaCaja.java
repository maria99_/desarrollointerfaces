package tpv;

import javax.swing.JFrame;

public class VentanaCaja {
	
	public static void main(String[] args) {
		MarcoTPV ventana = new MarcoTPV();
		
		// Crear la ventana y que sea Visible
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setVisible(true);

	}

}
