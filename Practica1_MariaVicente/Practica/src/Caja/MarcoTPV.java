package Caja;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoTPV extends JFrame{

	public Toolkit tool;
	public Dimension screenSize;
	
	public MarcoTPV() {
		
		tool = Toolkit.getDefaultToolkit();
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(screenSize.width, screenSize.height);
		
		this.setUndecorated(true);
		
		this.setLayout(new BorderLayout());
		
		PanelCaja panel = new PanelCaja();
		panel.setBackground(Color.WHITE);
		this.add(panel, BorderLayout.CENTER);
		
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
		
		
		

	}
	
}
