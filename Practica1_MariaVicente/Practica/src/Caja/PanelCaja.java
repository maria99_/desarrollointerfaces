package Caja;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

public class PanelCaja extends JPanel implements ActionListener{
	
	private JPanel panelProducto;
	private JPanel panelTexto;
	private JPanel panelNumeros;
	private JPanel panelTitulo;
	private JPanel contenedor5;
	
	private GridBagLayout layout;
	private GridBagConstraints config;
	
	private TextArea infoProductos;
	private TextArea numeroPulsado;
	private TextArea precio;
	private TextArea valor;
	
	private JButton btn0;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btn7;
	private JButton btn8;
	private JButton btn9;
	private JButton btnSalir;
	private JButton btnNuevo;
	
	private JButton btnEmpanada;
	private JButton btnFanta;
	private JButton btnPatatas;
	private JButton btnBocadillo;
	private JButton btnCalamares;
	private JButton btnCoca;
	private JButton btnNestea;
	private JButton btnCroquetas;
	private JButton btnCafe;
	private JButton btnJamon;
	private JButton btnNachos;
	private JButton btnFingers;
	
	private JTextArea cuadro1;
	private TextArea  cuadro2;
	private JTextArea cuadro3;
	private JTextArea cuadro4;
	private JTextArea cuadro5;

	public PanelCaja() {
		this.setBorder(new LineBorder(Color.BLACK, 2));
		layout = new GridBagLayout();
		config = new GridBagConstraints();
		
		panelProducto = new JPanel();
		panelTexto = new JPanel();
		panelNumeros = new JPanel();
		panelTitulo = new JPanel();
		contenedor5 = new JPanel();
		
		infoProductos = new TextArea();
		numeroPulsado = new TextArea();
		
		numeroPulsado.setBackground(Color.GRAY);
		
		precio = new TextArea();
		
		btn0 = new JButton("0");
		btn1 = new JButton("1");
		btn2 = new JButton("2");
		btn3 = new JButton("3");
		btn4 = new JButton("4");
		btn5 = new JButton("5");
		btn6 = new JButton("6");
		btn7 = new JButton("7");
		btn8 = new JButton("8");
		btn9 = new JButton("9");
		btnSalir = new JButton("Salir");
		btnNuevo = new JButton("Nuevo");
		
		btnEmpanada = new JButton(new ImageIcon("empanada.png"));
		btnFanta = new JButton(new ImageIcon("fanta.png"));
		btnPatatas = new JButton(new ImageIcon("patatas.png"));
		btnBocadillo = new JButton(new ImageIcon("bocadillo.png"));
		btnCalamares = new JButton(new ImageIcon("calamares.png"));
		btnCoca = new JButton(new ImageIcon("coca.png"));
		btnNestea = new JButton(new ImageIcon("nestea.png"));
		btnCroquetas = new JButton(new ImageIcon("croquetas.png"));
		btnCafe = new JButton(new ImageIcon("cafe.jpg"));
		btnJamon= new JButton(new ImageIcon("jamon.png"));
		btnNachos= new JButton(new ImageIcon("nachos.png"));
		btnFingers= new JButton(new ImageIcon("fingers.png"));
		
		this.setLayout(layout);
		agregarNumeros();
		agregarTexto();
		agregarProductos();
		agregarPanel();
	}


	private void agregarProductos() {
		panelProducto.setLayout(layout);
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 0;
		config.gridy = 0;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelProducto.add(btnEmpanada, config);
		btnEmpanada.addActionListener((ActionListener) this);
		
		
		config.gridx = 1;
		config.gridy = 0;
		panelProducto.add(btnFanta, config);
		btnFanta.addActionListener((ActionListener) this);
		
		
		config.gridx = 2;
		config.gridy = 0;
		panelProducto.add(btnPatatas, config);
		btnPatatas.addActionListener((ActionListener) this);
		
		
		config.gridx = 3;
		config.gridy = 0;
		panelProducto.add(btnBocadillo, config);
		btnBocadillo.addActionListener((ActionListener) this);
		
		
		config.gridx = 0;
		config.gridy = 1;
		panelProducto.add(btnCalamares, config);
		btnCalamares.addActionListener((ActionListener) this);
		
		
		config.gridx = 1;
		config.gridy = 1;
		panelProducto.add(btnCoca, config);
		btnCoca.addActionListener((ActionListener) this);
		
		
		config.gridx = 2;
		config.gridy = 1;
		panelProducto.add(btnNestea, config);
		btnNestea.addActionListener((ActionListener) this);
		
		
		config.gridx = 3;
		config.gridy = 1;
		panelProducto.add(btnCroquetas, config);
		btnCroquetas.addActionListener((ActionListener) this);
		
		
		config.gridx = 0;
		config.gridy = 3;
		panelProducto.add(btnCafe, config);
		btnCafe.addActionListener((ActionListener) this);
		
		
		config.gridx = 1;
		config.gridy = 3;
		panelProducto.add(btnJamon, config);
		btnJamon.addActionListener((ActionListener) this);
		
		
		config.gridx = 2;
		config.gridy = 3;
		panelProducto.add(btnNachos, config);
		btnNachos.addActionListener((ActionListener) this);
		
		
		config.gridx = 3;
		config.gridy = 3;
		panelProducto.add(btnFingers, config);
		btnFingers.addActionListener((ActionListener) this);
	}


	private void agregarTexto() {
		panelTexto.setLayout(layout);
		
		config.fill = GridBagConstraints.HORIZONTAL;
		config.gridx = 0;
		config.gridy = 0;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 0.0;
		
		infoProductos.setBackground(Color.WHITE);
		infoProductos.setForeground(Color.BLACK);
		infoProductos.setFont(new Font("Arial", Font.BOLD, 15));
		panelTexto.add(infoProductos, config);

		config.fill = GridBagConstraints.HORIZONTAL;
		config.gridx = 1;
		config.gridy = 0;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 0.0;
		config.weighty = 0.0;
		
		precio.setBackground(Color.WHITE);
		precio.setForeground(Color.BLACK);
		precio.setFont(new Font("Arial", Font.BOLD, 15));
		panelTexto.add(precio, config);

	}

	

	private void agregarNumeros() {
		panelNumeros.setLayout(layout);

		config.fill = GridBagConstraints.HORIZONTAL;
		config.gridx = 0;
		config.gridy = 1;
		config.gridwidth = 3;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 0.0;
		panelNumeros.add(numeroPulsado, config);

		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 0;
		config.gridy = 2;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn1, config);
		btn1.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 1;
		config.gridy = 2;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn2, config);
		btn2.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 2;
		config.gridy = 2;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn3, config);
		btn3.addActionListener((ActionListener) this);

		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 0;
		config.gridy = 3;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn4, config);
		btn4.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 1;
		config.gridy = 3;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn5, config);
		btn5.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 2;
		config.gridy = 3;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn6, config);
		btn6.addActionListener((ActionListener) this);

		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 0;
		config.gridy = 4;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn7, config);
		btn7.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 1;
		config.gridy = 4;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn8, config);
		btn8.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 2;
		config.gridy = 4;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn9, config);
		btn9.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 0;
		config.gridy = 5;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btn0, config);
		btn0.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 1;
		config.gridy = 5;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btnNuevo, config);
		btnNuevo.addActionListener((ActionListener) this);
		
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 2;
		config.gridy = 5;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		panelNumeros.add(btnSalir, config);
		btnSalir.addActionListener((ActionListener) this);
	}

	private void agregarPanel() {
		
		config.fill = GridBagConstraints.HORIZONTAL;
		config.gridx = 0;
		config.gridy = 0;
		config.gridwidth = 2;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 0.0;
		this.add(panelTexto, config);
		
		config.fill = GridBagConstraints.BOTH;
		config.gridx = 0;
		config.gridy = 1;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 1.0;
		config.weighty = 1.0;
		this.add(panelProducto, config);
		
		config.fill = GridBagConstraints.VERTICAL;
		config.gridx = 1;
		config.gridy = 1;
		config.gridwidth = 1;
		config.gridheight = 1;
		config.weightx = 0.0;
		config.weighty = 0.0;
		this.add(panelNumeros, config);

	}


	@Override
	public void actionPerformed(ActionEvent e) {
		double suma = 0;
		int totalProductos;
		int sumatorio;
		int cantidad = 0;
		double total=0;
		
		if(e.getSource() == btn0) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "0");
		}
		if(e.getSource() == btn1) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "1");
		}
	
		if(e.getSource() == btn2) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "2");
		}
		
		if(e.getSource() == btn3) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "3");
		}
		
		if(e.getSource() == btn4) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "4");
		}
		
		if(e.getSource() == btn5) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "5");
		}
		
		if(e.getSource() == btn6) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "6");
		}
		
		if(e.getSource() == btn7) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "7");
		}
		
		if(e.getSource() == btn8) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "8");
		}
		
		if(e.getSource() == btn9) {
			valor = numeroPulsado;
			numeroPulsado.setText(valor.getText() + "9");
		}
		
		if(e.getSource() == btnNuevo) {
			infoProductos.setText("");	
			numeroPulsado.setText("");	
			precio.setText("0");
			valor.setText("0");
			
		}
		
		if(e.getSource() == btnSalir) {
			System.exit(0);
		}
		
		if(e.getSource() == btnCafe) {
			double precioCafe = 1.20;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Cafe 1.20------>  " +
			(precioCafe*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
			
			numeroPulsado.setText("");
			
		}		
		
		if(e.getSource() == btnBocadillo) {
			double precioBocata = 5;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Bocadillo 5  ------>  " +
			(precioBocata*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
			
			numeroPulsado.setText("");
		}
		
		if(e.getSource() == btnCalamares) {
			double precioCalamar = 4;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Calamares 4 ------>  " +
			(precioCalamar*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
			
			numeroPulsado.setText("");
			
		}
		
		if(e.getSource() == btnCoca) {
			double precioCoca = 2;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Coca-Cola  2 ------>  " +
			(precioCoca*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
			
			numeroPulsado.setText("");
		}
		
		if(e.getSource() == btnCroquetas) {
			double precioCroqueta = 2.30;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Croqueta  2.30 ------>  " +
			(precioCroqueta*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
			
			numeroPulsado.setText("");
							
		}
		
		if(e.getSource() == btnEmpanada) {
			double precioEmpanada= 1.50;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Empanada  1.50 ------>  " +
			(precioEmpanada*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
			
			numeroPulsado.setText("");
		}
		
		if(e.getSource() == btnFanta) {
			double precioFanta = 1.20;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Fanta  1.20 ------>  " +
			(precioFanta*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
					
			numeroPulsado.setText("");
		}
		
		if(e.getSource() == btnFingers) {
			double precioFingers = 3.40;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Fingers de Pollo  3.40 ------>  " +
			(precioFingers*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
							
			numeroPulsado.setText("");
		}
		
		if(e.getSource() == btnJamon) {
			double precioJamon = 6;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Jamon Serrano  6 ------>  " +
			(precioJamon*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
							
			numeroPulsado.setText("");
		}
		
		if(e.getSource() == btnNestea) {
			double precioNestea = 1.50;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Nestea  1.50 ------>  " +
			(precioNestea*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
							
			numeroPulsado.setText("");
		}
		
		if(e.getSource() == btnPatatas) {
			double precioPatatas = 3;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Patatas Bravas 3 ------>  " +
			(precioPatatas*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
					
			numeroPulsado.setText("");
		}
		
		if(e.getSource() == btnNachos) {
			double precioNachos = 4;
			valor = infoProductos;
			infoProductos.setText(valor.getText() + " "+(numeroPulsado.getText()) + " Nachos  4 ------>  " +
			(precioNachos*Double.parseDouble(numeroPulsado.getText())) + "�" + "\n");
							
			numeroPulsado.setText("");
		}
		
	}

}
	
	